package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
// annotation-under the hood codes to be used
@RestController
// handles the endpoint for web requests
public class Wdc044Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}

	@GetMapping("/hi")
	// GetMapping - tells Spring to use this method when a GET request is received
	public String hello(@RequestParam(value = "user", defaultValue="user")String user){
	// RequestParam - annotation tells Spring to expect a new value in the request but if it's not there, a default value will be used
		return String.format("Hi %s!", user);
		// %s- any type but the return is a String
		// $c- character and the output is a unicode character
		// %b- any type but the output is boolean
	}
	@GetMapping("/nameage")
	public String nameAge(@RequestParam(value = "name", defaultValue = "Juan") String name, @RequestParam(value = "age", defaultValue = "18") String age){
		return String.format("Hello %s! Your age is %s.", name, age);
	}

}
// ./mvnw spring-boot:run
// with multiple parameters: localhost:8080/nameage?parameter=value&parameter=value
