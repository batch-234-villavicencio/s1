package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.Post;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
// this is responsible for the CRUD operations
public interface PostRepository extends CrudRepository<Post, Object> {

    @Query("SELECT id FROM posts JOIN users ON posts.user_id = users.id WHERE username LIKE CONCAT('%', :stringToken, '%')")
    Long author_id(String stringToken);

}
