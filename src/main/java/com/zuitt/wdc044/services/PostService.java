package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ResponseStatus;

public interface PostService {
    // create a post
    void createPost(String stringToken, Post post);

    ResponseEntity updatePost(Long id, String stringToken, Post post);

    ResponseEntity deletePost(Long id, String stringToken);

    Iterable<Post> getMyPosts(String stringToken);

    // retrieve posts
    Iterable<Post> getPosts();


}
